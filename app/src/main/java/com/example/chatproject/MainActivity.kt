package com.example.chatproject

import Tickerr
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.activity_main.*
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.lang.Exception
import java.net.URI
import javax.net.ssl.SSLSocketFactory

class MainActivity : AppCompatActivity() {
    private lateinit var webSocketClient: WebSocketClient
    val url = "wss://ws-feed.pro.coinbase.com"
    val TAG = "errror"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private fun setUpBtcPriceText(message: String?) {
        Log.d("errror", message!!)
        message?.let {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<Tickerr> = moshi.adapter(Tickerr::class.java)
            val bitcoin = adapter.fromJson(message)
            runOnUiThread { textView.text = "1 BTC: ${bitcoin?.price} €" }
        }
    }

    private fun subscribe() {
        webSocketClient.send(
            "{\n" +
                    "    \"type\": \"subscribe\",\n" +
                    "    \"channels\": [{ \"name\": \"ticker\", \"product_ids\": [\"BTC-EUR\"] }]\n" +
                    "}"
        )
    }

    private fun createWebSocketClient(coinbaseUri: URI?) {
        webSocketClient = object : WebSocketClient(coinbaseUri) {
            override fun onOpen(handshakedata: ServerHandshake?) {
                Log.d(TAG, "onOpen")
                subscribe()
            }

            override fun onMessage(message: String?) {
                Log.d(TAG, "onMessage")
                setUpBtcPriceText(message)
            }

            override fun onClose(code: Int, reason: String?, remote: Boolean) {
                Log.d(TAG, "onClose")
            }

            override fun onError(ex: Exception?) {
                Log.d(TAG, "onError" + ex!!.message)
            }

        }
    }

    private fun initWebSocket() {
        val coinbaseUri: URI? = URI(url)

        val socketFactory: SSLSocketFactory = SSLSocketFactory.getDefault() as SSLSocketFactory

        createWebSocketClient(coinbaseUri)
        webSocketClient.setSocketFactory(socketFactory)
        webSocketClient.connect()
    }

    override fun onResume() {
        super.onResume()
        initWebSocket()
    }

    override fun onPause() {
        super.onPause()
        webSocketClient.close()
    }
}